package sh.keith.ocr;

import sh.keith.ocr.model.InvalidOcrCharacterException;
import sh.keith.ocr.model.OcrFormattingException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static sh.keith.ocr.model.OcrNumbers.*;

public class OcrReader {
    /**
     * The number of lines there should be in a valid OCR input (Should be considered 4, but the final empty line will not register)
     */
    private static final int OCR_LINE_COUNT = 3;
    private static final int OCR_LINE_LENGTH = 27;

    private static final int ILLEGIBLE_DIGIT = -1;

    /**
     * Read a raw OCR input account name to its corresponding numeric values
     *
     * @param ocrContent The raw OCR content for the account number
     * @return The account number in the format of its numbers in an integer ArrayList
     * @throws OcrFormattingException If there is any invalid formatting in the OCR input
     */
    public List<Integer> readOcrToDigits(final String ocrContent) throws OcrFormattingException {
        String[] lines = ocrContent.split("\n");
        if (lines.length != OCR_LINE_COUNT) {
            throw new OcrFormattingException("Wrong number of lines in OCR file");
        }
        // Init operating under the assumption all lines are equal length, will verify while parsing
        StringBuilder[] numbers = new StringBuilder[OCR_LINE_LENGTH / 3];

        for (final String line : lines) {
            // Verify that all lines are the same length and divisible by 3
            if (line.length() != OCR_LINE_LENGTH) {
                throw new OcrFormattingException("Invalid OCR input line length");
            }
            int digit = 0;
            for (int c = 0; c < OCR_LINE_LENGTH; c += 3) {
                if (numbers[digit] == null) {
                    numbers[digit] = new StringBuilder();
                }
                numbers[digit].append(line.charAt(c));
                numbers[digit].append(line.charAt(c + 1));
                numbers[digit].append(line.charAt(c + 2));
                digit++;
            }
        }

        final List<Integer> values = new ArrayList<>();

        for (final StringBuilder numberBuilder : numbers) {
            final String number = numberBuilder.toString();
            int value;
            try {
                value = matchString(number);
            } catch (OcrFormattingException e) {
                // This is not a valid OCR input. Store -1 so that it can be handled down the line
                value = ILLEGIBLE_DIGIT;
            }
            values.add(value);
        }

        return values;
    }

    /**
     * Validate the checksum of this account number. Returning in the negative since that is how this function will most frequently be called
     *
     * @param accountNumber The account number to validate
     * @return whether this account number is invalid
     */
    private static boolean hasInvalidChecksum(final List<Integer> accountNumber) {
        // Don't allow valid checksum on illegible account number
        for (int i : accountNumber) {
            if (i == ILLEGIBLE_DIGIT) {
                return true;
            }
        }
        return (accountNumber.get(8) +
                2 * accountNumber.get(7) +
                3 * accountNumber.get(6) +
                4 * accountNumber.get(5) +
                5 * accountNumber.get(4) +
                6 * accountNumber.get(3) +
                7 * accountNumber.get(2) +
                8 * accountNumber.get(1) +
                9 * accountNumber.get(0))
                % 11 != 0;
    }

    /**
     * Print the account number and any errors that were encountered while parsing or validating it
     * NOTE: This is the solution for Story 3. I will duplicate this function for Story 4, so that they can be tested separately. Sorry for the duplicated code.
     *
     * @param ocrInput Raw OCR input of the Account number
     * @return Spring representation of the OCR Account number
     * @throws OcrFormattingException If the OCR input is invalid
     */
    public String printAccountNumber(final String ocrInput) throws OcrFormattingException {
        final List<Integer> accountNumber = this.readOcrToDigits(ocrInput);
        final StringBuilder output = new StringBuilder();
        output.append(accountNumberToString(accountNumber));
        if (hasInvalidChecksum(accountNumber) && allCharsLegible(accountNumber)) {
            // Invalid checksum
            output.append(" ERR");
        }
        return output.toString();
    }


    /**
     * Print the account number and any errors that were encountered while parsing or validating it
     * NOTE: This is the solution for Story 4. It was duplicated from the solution from Story 3. Sorry for the duplicated code.
     *
     * @param ocrInput Raw OCR input of the Account number
     * @return Spring representation of the OCR Account number
     * @throws OcrFormattingException If the OCR input is invalid
     */
    public String printAccountNumberFixed(final String ocrInput) throws OcrFormattingException {
        List<List<Integer>> fixes = new ArrayList<>();
        List<Integer> accountNumber = this.readOcrToDigits(ocrInput);
        final StringBuilder output = new StringBuilder();

        // Check to see if this list has a valid checksum, and if not, find a potential new one
        if (hasInvalidChecksum(accountNumber)) {
            final List<List<Integer>> calculatedFixes = findPossibleFixes(accountNumber);
            if (calculatedFixes.size() == 1) {
                accountNumber = calculatedFixes.get(0);
            } else if (calculatedFixes.size() > 1) {
                fixes.addAll(calculatedFixes);
            }
            
        }
        output.append(accountNumberToString(accountNumber));
        if (hasInvalidChecksum(accountNumber) && allCharsLegible(accountNumber)) {
            if (fixes.size() > 1) {
                output.append(" AMB [");
                output.append(fixes.stream()
                        .map(OcrReader::accountNumberToString)
                        .map(s -> String.format("'%s'", s))
                        .collect(Collectors.joining(",")));
                output.append("]");
            } else {
                output.append(" ERR");
            }
        }
        return output.toString();
    }
    
    private static boolean allCharsLegible(final List<Integer> accountNumber) {
        for (int x : accountNumber) {
            if (x == ILLEGIBLE_DIGIT) {
                return false;
            }
        }
        return true;
    }

    private static String accountNumberToString(final List<Integer> accountNumber) {
        final StringBuilder output = new StringBuilder();
        boolean ill = false;
        for (final int x : accountNumber) {
            if (x == ILLEGIBLE_DIGIT) {
                // Was an invalid digit, report as a question mark
                output.append("?");
                ill = true;
            } else {
                output.append(x);
            }
        }
        if (ill) {
            output.append(" ILL");
        }
        return output.toString();
    }

    private static List<List<Integer>> findPossibleFixes(final List<Integer> accountNumber) {
        final List<List<Integer>> fixes = new ArrayList<>();
        for (int i = 0; i < accountNumber.size(); i++) {
            final int[] alts = closeNumbers(accountNumber.get(i));
            for (int alternativeDigit : alts) {
                final List<Integer> possibleFix = new ArrayList<>(accountNumber);
                possibleFix.set(i, alternativeDigit); // Put the new potential digit in place of the old one, then validate checksum
                if (!hasInvalidChecksum(possibleFix)) {
                    fixes.add(possibleFix);
                }
            }
        }
        return fixes;
    }


    /**
     * Find all digits that are only one pipe or underscore modification from this one
     *
     * @param digit the digit to find possible alternatives for
     * @return the full list of potential alternatives for this digit
     */
    private static int[] closeNumbers(final int digit) {
        return switch (digit) {
            case 0 -> new int[]{8};
            case 1 -> new int[]{7};
            case 3 -> new int[]{9};
            case 5 -> new int[]{9, 6};
            case 6 -> new int[]{5, 8};
            case 7 -> new int[]{1};
            case 8 -> new int[]{0, 6, 9};
            case 9 -> new int[]{3, 5, 8};
            default -> new int[]{};
        };
    }/**/

    /**
     * Match the String representation of an OCR number with its corresponding numeric equivalent
     *
     * @param ocrValue 9 Character long representation of the OCR input to be deciphered
     * @return the numeric equivalent of the OCR input
     * @throws InvalidOcrCharacterException If the input does not match any valid OCR values
     */
    private static int matchString(final String ocrValue) throws InvalidOcrCharacterException {
        return switch (ocrValue) {
            case ZERO -> 0;
            case ONE -> 1;
            case TWO -> 2;
            case THREE -> 3;
            case FOUR -> 4;
            case FIVE -> 5;
            case SIX -> 6;
            case SEVEN -> 7;
            case EIGHT -> 8;
            case NINE -> 9;
            default -> throw new InvalidOcrCharacterException("Could not identify OCR Character");
        };
    }
}
