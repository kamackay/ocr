package sh.keith.ocr.model;

public class OcrFormattingException extends Exception {

    public OcrFormattingException(String message) {
        super(message);
    }
}
