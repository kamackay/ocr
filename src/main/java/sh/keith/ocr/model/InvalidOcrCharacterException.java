package sh.keith.ocr.model;

public class InvalidOcrCharacterException extends OcrFormattingException {
    public InvalidOcrCharacterException(String message) {
        super(message);
    }
}
