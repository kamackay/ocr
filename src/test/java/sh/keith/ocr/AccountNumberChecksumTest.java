package sh.keith.ocr;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import sh.keith.ocr.model.OcrFormattingException;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountNumberChecksumTest {

    @ParameterizedTest
    @ArgumentsSource(ArgsProvider.class)
    void verifyChecksum(String ocrInput, String expected) throws OcrFormattingException {
        final OcrReader reader = new OcrReader();
        final String value = reader.printAccountNumber(ocrInput);
        assertEquals(expected, value);
    }

    static class ArgsProvider implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            return Stream.of(
                    Arguments.of("""
                             _  _  _  _  _  _  _  _   \s
                            | || || || || || || ||_   |
                            |_||_||_||_||_||_||_| _|  |
                            """, "000000051"),
                    Arguments.of("""
                                _  _  _  _  _  _     _\s
                            |_||_|| || ||_   |  |  | _\s
                              | _||_||_||_|  |  |  | _|
                              """, "49006771? ILL"),
                    Arguments.of("""
                                _  _     _  _  _  _  _\s
                              | _| _||_| _ |_   ||_||_|
                              ||_  _|  | _||_|  ||_| _\s
                            """, "1234?678? ILL"),
                    Arguments.of("""
                                                      \s
                              |  |  |  |  |  |  |  |  |
                              |  |  |  |  |  |  |  |  |
                            """, "111111111 ERR")
            );
        }
    }

}
