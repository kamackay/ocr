package sh.keith.ocr;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import sh.keith.ocr.model.OcrFormattingException;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountNumberValidationTest {

    @ParameterizedTest
    @ArgumentsSource(ArgsProvider.class)
    void checkAccountNumber(String ocrInput, List<Integer> expected) throws OcrFormattingException {
        final OcrReader reader = new OcrReader();
        final List<Integer> values = reader.readOcrToDigits(ocrInput);
        for (int i = 0; i < values.size(); i++) {
            assertEquals(expected.get(i), values.get(i));
        }
    }

    static class ArgsProvider implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            return Stream.of(
                    Arguments.of(
                            """
                                     _  _  _  _  _  _  _  _  _\s
                                    | || || || || || || || || |
                                    |_||_||_||_||_||_||_||_||_|
                                    """,
                            asList(0, 0, 0, 0, 0, 0, 0, 0, 0)),
                    Arguments.of(
                            """
                                                              \s
                                      |  |  |  |  |  |  |  |  |
                                      |  |  |  |  |  |  |  |  |
                                    """,
                            asList(1, 1, 1, 1, 1, 1, 1, 1, 1)),
                    Arguments.of("""
                             _  _  _  _  _  _  _  _  _\s
                             _| _| _| _| _| _| _| _| _|
                            |_ |_ |_ |_ |_ |_ |_ |_ |_\s
                            """, asList(2, 2, 2, 2, 2, 2, 2, 2, 2)),
                    Arguments.of("""
                             _  _  _  _  _  _  _  _  _\s
                             _| _| _| _| _| _| _| _| _|
                             _| _| _| _| _| _| _| _| _|
                            """, asList(3, 3, 3, 3, 3, 3, 3, 3, 3)),
                    Arguments.of("""
                                                      \s
                            |_||_||_||_||_||_||_||_||_|
                              |  |  |  |  |  |  |  |  |
                            """, asList(4, 4, 4, 4, 4, 4, 4, 4, 4)),
                    Arguments.of("""
                             _  _  _  _  _  _  _  _  _\s
                            |_ |_ |_ |_ |_ |_ |_ |_ |_\s
                             _| _| _| _| _| _| _| _| _|
                             """, asList(5, 5, 5, 5, 5, 5, 5, 5, 5)),
                    Arguments.of("""
                             _  _  _  _  _  _  _  _  _\s
                            |_ |_ |_ |_ |_ |_ |_ |_ |_\s
                            |_||_||_||_||_||_||_||_||_|
                            """, asList(6, 6, 6, 6, 6, 6, 6, 6, 6)),
                    Arguments.of("""
                             _  _  _  _  _  _  _  _  _\s
                              |  |  |  |  |  |  |  |  |
                              |  |  |  |  |  |  |  |  |
                            """, asList(7, 7, 7, 7, 7, 7, 7, 7, 7)),
                    Arguments.of("""
                             _  _  _  _  _  _  _  _  _\s
                            |_||_||_||_||_||_||_||_||_|
                            |_||_||_||_||_||_||_||_||_|
                            """, asList(8, 8, 8, 8, 8, 8, 8, 8, 8)),
                    Arguments.of("""
                             _  _  _  _  _  _  _  _  _\s
                            |_||_||_||_||_||_||_||_||_|
                             _| _| _| _| _| _| _| _| _|
                             """, asList(9,9,9,9,9,9,9,9,9))
            );
        }
    }

}
