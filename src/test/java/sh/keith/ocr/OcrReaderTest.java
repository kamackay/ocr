package sh.keith.ocr;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import sh.keith.ocr.model.OcrFormattingException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OcrReaderTest {

    private static OcrReader reader;

    @BeforeAll
    public static void setup() {
        reader = new OcrReader();
    }

    @Test
    public void allOcrNumbersValid() throws IOException, OcrFormattingException {
        String ocrText = readInFile("ocr_in_order.txt");
        final List<Integer> values = reader.readOcrToDigits(ocrText);
        for (int i = 0; i < 9; i++) {
            assertEquals(i + 1, values.get(i));
        }
    }

    private String readInFile(final String filename) throws IOException {
        try (final InputStream inputStream = this.getClass().getResourceAsStream(filename)) {
            return new String(Objects.requireNonNull(inputStream).readAllBytes(), StandardCharsets.UTF_8);
        }
    }

}